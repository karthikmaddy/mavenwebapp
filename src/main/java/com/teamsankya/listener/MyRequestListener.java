package com.teamsankya.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class MyRequestListener implements ServletRequestListener {

    
    public MyRequestListener() {
        
    }

    public void requestDestroyed(ServletRequestEvent arg0)  { 
        System.out.println("Destroyed");
    }

	
    public void requestInitialized(ServletRequestEvent arg0)  { 
         System.out.println("this is init");
    }
	
}
